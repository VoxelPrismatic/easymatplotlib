import matplotlib as mpl
import numpy as np
import numexpr as ne
import matplotlib.pyplot as plt

def make(): return plt.figure()
def subplots(): return plt.subplots()
def x_range(xmin:int,xmax:int): return np.array(range(xmin,xmax))
def graph(eq,x_range): plt.plot(x_range,ne.evaluate(eq.replace('^','**')))
def saveimg(form): name = io.BytesIO(); return plt.savefig(name,format=form).seek(0)
    